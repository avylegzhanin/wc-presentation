```js
    customElements.define(
        /* @{string} */     name, 
        /* @{Class} */      constructor, 
        /* @{Object} */     options = { extends: '' }
    )
```
```js
    customElements.get(
        /* @{string} */     name, 
    )
```
```js
    customElements.whenDefined(
        /* @{string} */     name ||
        /* @{[string]} */   ...names
    )
```