## Slots

```js
    /* ... */
    this.attachShadow({mode: 'open'})
        .innerHTML = `
            <div>I'm inner div</div>
            
            <slot name="heading-text"></slot>
            <slot name="content"></slot>
            <slot name="action-bar"></slot>`;
    /* ... */
```
```html
    <!-- ... -->
    <some-custom-element>
        <!-- div -->
        <h2 slot="headint-text">Some header</h2>
        <p slot="content">Some paragraph content</p>
        <another-custom-element slot="action-bar"></another-custom-element>
    </some-custom-element>
    <!-- ... -->
```