## Slots

```html
    <!-- ... -->
    <some-custom-element>
        <h2>Some header</h2>
        <p>Some paragraph content</p>
        <another-custom-element></another-custom-element>
    </some-custom-element>
    <!-- ... -->
```