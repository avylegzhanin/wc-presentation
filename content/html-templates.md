## Templates 📃

`index.html`
```html
    <!-- ... -->
    <template id="someTemplate">
        <h2>I'm template tag</h2>
        <p>There is some content inside</p>
        
        <also-custom-element></also-custom-element>
    </template>
    <!-- ... -->
```
`main.js`
```js
    /* ... */
    const template = document.querySelector('#someTemplate');
    const templateContent = template.content.cloneNode(true);
    /* ... */
```