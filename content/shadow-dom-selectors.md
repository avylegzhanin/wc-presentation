## Styling &#9819;
```css
   :host { display: block; }

   :host(.grey) {/*  */}
   :host([attribute="property"]) {/*  */}
   :host(:hover) {/*  */}
   :host(:disabled) {/*  */}
   :host(::after) {/*  */}

   :host-context(.body--theme-light) { background: #fff; }

   <child-selector-1> { /*  */ }
   <child-selector-2> { /*  */ }
```