`my-custom.element.js`
```js
    class MyCustomElement extends HTMLElement {
        constructor() { super(); /*  */}
        
        static get observedAttributes() {return ['attName'];}
        
        attributeChangedCallback(attrName, oldVal, newVal) {/*  */}
        
        connectedCallback() {/*  */}

        disconnectedCallback() {/*  */}

        adoptedCallback() {/*  */}
    }

    customElements.define('my-custom-element', MyCustomElement);
```