```js
    /*  */
    class MyCustomElement extends HTMLElement {
        constructor() { 
            super();
            this.root = this.attachShadow({mode: 'open'});
            /*  */
            this.root.innerHTML = '';
            this.root.appendChild(someNodeElement)
        }
    }
    /*  */
```
[my custom element](../examples/test-custom-elements-with-shadow.html)