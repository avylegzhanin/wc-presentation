## HTML Imports 📦

`form.html`
```html
    <form>
        <label for="#fullName">Fullname:</label><input type="text" id="fullName" /><!-- ... -->
        <label for="#age">Age:</label><input type="text" id="age" /><!-- ... -->
        <label for="#gender">Genger:</label><select id="gender"><!-- ... --></select>
        <fancy-button mode="inline" text="Click to submit"></fancy-button>
    </form>
```
`index.html`
```html
    <body>
        <!-- Some markdown here -->

        <link rel="import" href="../../form.html">
        
        <!-- And here -->
    </body>
```