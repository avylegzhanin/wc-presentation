`my-custom.element.js`
```js
    class MyCustomElement extends HTMLElement {
        constructor() { 
            super();
            this.innerHTML = `
                <h2>Custom 'Hello World!'</h2>
            `;
            
            const paragraph = document.createElement('p');
            paragraph.textContent = 'Some paragraph stuff';

            this.appendChild(paragraph);
        }
    }

    customElements.define('my-custom-element', MyCustomElement);
```
[link](../examples/test-custom-elements.html)