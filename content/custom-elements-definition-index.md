## Custom elements &#128295;

`index.html`
```html
    <body>
        <h2>Hello world!</h2>
        <!-- ... -->
        <my-custom-element></my-custom-element>
        <!-- ... -->
        
        <script src="./my-custom-element.js"></script>
    </body>
```